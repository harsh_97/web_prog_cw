from django.urls import path

from match import views


# url patterns for the system
urlpatterns = [
    path('', views.login, name='login'),
    path('signup/', views.signup, name='signup'),
    path('register/', views.register, name='register'),
    path('login/', views.login, name='login'),
    path('matches/', views.matches, name='matches'),
    path('profile/', views.self_profile, name='profile'),
    path('update_profile', views.update_profile, name='update_profile'),
    path('homepage/', views.homepage, name='homepage'),
    path('messages/', views.messages, name='messages'),
    path('add_hobbies/', views.self_profile, name='add_hobbies'),
    path('logout/', views.logout, name='logout'),
]
