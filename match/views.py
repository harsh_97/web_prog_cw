from django.db import IntegrityError
from django.http import Http404, HttpResponse
from django.shortcuts import render

from match.models import Member, Message

from datetime import datetime

import datetime

app_name = 'MatchIt'
# Note: the address the project is hosted on is http://206.189.16.157:8000 on DigitalOcean.


# decorator to check that user is logged in for pages which require a logged in session
def is_logged_in(view):
    def mod_view(request):
        if 'username' in request.session:
            username = request.session['username']
            try:
                user = Member.objects.get(username=username)
            except Member.DoesNotExist:
                raise Http404('Member does not exist')
            return view(request, user)
        else:
            return render(request, 'match/not_logged_in.html', {})

    return mod_view


# A simple redirect view
def signup(request):
    context = {'app_name': app_name}
    return render(request, 'match/sign_up.html', context)


# The view which registers the user from the signup page with POST for member model attributes
def register(request):
    email = request.POST['username']
    password = request.POST['password']
    member = Member(username=email)
    member.name = request.POST['display_name']
    member.set_password(password)
    member.dob = request.POST['dob']
    dob = datetime.datetime.strptime(request.POST['dob'], '%Y-%m-%d')
    today = datetime.datetime.today()
    age = int(round((today - dob).days / 365))
    member.age = age
    # the above time code is for calculating age from dob which is used in the card stack which lists member details
    member.gender = request.POST['gender']
    try:
        member.save()
    except IntegrityError:
        raise Http404('Username ' + email + ' is already taken')
    context = {
        'app_name': app_name,
        'username': email
    }
    return render(request, 'match/registered.html', context)


# A view where the authentication occurs, it will create a session cookie for the user and render the main homepage
# once logged in
def login(request):
    if not ('username' in request.POST and 'password' in request.POST):
        context = {'app_name': app_name}
        return render(request, 'match/login.html', context)
    else:
        username = request.POST['username']
        password = request.POST['password']
        try:
            member = Member.objects.get(username=username)
            members = Member.objects.exclude(username=member.username)
        except Member.DoesNotExist:
            raise Http404('Member does not exist')
        if member.check_password(password):
            request.session['username'] = username
            request.session['password'] = password
            context = {
                'app_name': app_name,
                'username': username,
                'members': members,
                'is_logged_in': True
            }
            response = render(request, 'match/homepage.html', context)
            now = datetime.datetime.utcnow()
            max_age = 365 * 24 * 60 * 60 # cookie expiration 1 year
            delta = now + datetime.timedelta(seconds=max_age)
            format = "%a, %d-%b-%Y %H:%M:%S GMT"
            expires = datetime.datetime.strftime(delta, format)
            response.set_cookie('last_login', now, expires=expires)
            return response
        else:
            raise Http404('Incorrect password')


# view to redirect user to logged out page
@is_logged_in
def logout(request, member):
    request.session.flush()
    context = {'app_name': app_name}
    return render(request, 'match/logout.html', context)


# this is the view called when the match it button is clicked to determine whether the members have matched
@is_logged_in
def matches(request, member):
    '''member.liked = member.liked + ', ' + str('b@b.com')
    liked = member.liked
    liked_list = liked.split(', ')
    for person in liked_list:
        m = Member.objects.get(username=str(person))
        m_liked = m.liked
        m_liked_list = m_liked.split(', ')
        # check both users to see whether they have liked each other
        for m_person in m_liked_list:
            mem = Member.objects.get(username=m_person)
            if mem == member:
                member.matched = member.matched + ', ' + str(m_liked)
                context = {
                    'app_name': app_name,
                    'username': member.username,
                    'matched': member.matched,
                    'is_logged_in': True
                }
                # render the matches page which lists all matches
                return render(request, 'match/matches.html', context)'''
    context = {
        'app_name': app_name,
        'username': member.username,
        # 'matches': matches,
        'is_logged_in': True
    }
    return render(request, 'match/matches.html', context)


# This view will show the other member's profile
@is_logged_in
def view_profile(request, view_member):
    username = request.session['username']
    try:
        member = Member.objects.get(username=view_member)
    except Member.DoesNotExist:
        raise Http404('Member does not exist')
    context = {
        'appname': app_name,
        'username': username,
        'view_member': view_member,
        # 'profile': member.profile,
        'is_logged_in': True
    }
    return render(request, 'match/member.html', context)


# this view shows the current users (who is logged in) profile
@is_logged_in
def self_profile(request, member):
    if request.POST and 'hobbies[]' in request.POST:
        # a list of hobbies is obtained and saved from the edit profile page
        member.hobbies = request.POST.getlist('hobbies[]')
        hobbies = member.hobbies
        member.save()
        context = {
            'app_name': app_name,
            'name': member.name,
            'age': member.age,
            'hobbies': hobbies,
            'image': member.image,
            'is_logged_in': True
        }
        return render(request, 'match/profile.html', context)
    hobbies = member.hobbies
    context = {
        'app_name': app_name,
        'name': member.name,
        'age': member.age,
        'hobbies': hobbies,
        'image': member.image,
        'is_logged_in': True
    }
    return render(request, 'match/profile.html', context)


# The main homepage where all users are listed
@is_logged_in
def homepage(request, member):
    # this is where the filter is applied as set by the user according to age and gender
    if request.POST and 'gender' in request.POST and 'minAge' in request.POST and 'maxAge' in request.POST:
        gender = request.POST['gender']
        min_age = int(request.POST['minAge'])
        max_age = int(request.POST['maxAge'])
        # filtering occurs
        members = Member.objects.filter(gender=gender).exclude(username=member.username).filter(age__range=(min_age,max_age))

    else:
        members = Member.objects.exclude(username=member.username)
        # new homepage is rendered with filters applied (if applied)
    return render(request, 'match/homepage.html', {
        'app_name': app_name,
        'username': member.username,
        'member': member,
        'members': members,
        # 'profile': member.profile,
        # 'view': view,
        'is_logged_in': True
    })


# the main view for messaging
@is_logged_in
def messages(request, member):
    context={'member': member}
    return render(request, 'match/messages.html', context)


# the view which updates the member's name, password and profile picture
@is_logged_in
def update_profile(request, member):
    if request.method == 'POST':
        if 'name' in request.POST and request.POST['name'] != '':
            member.name = request.POST['name']
        if 'password' in request.POST and request.POST['password'] != '':
            member.set_password(request.POST['password'])
        if 'img_file' in request.POST:
            image_file = request.POST['img_file']
            member.image = image_file
        member.save()
    context = {
        'app_name': app_name,
        'name': member.name,
        'age': member.age,
        'password': member.password,
        'username': member.username,
        'is_logged_in': True
    }
    return render(request, 'match/homepage.html', context)

