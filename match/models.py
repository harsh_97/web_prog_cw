from django.contrib.auth.models import User
from django.db import models

# Main user class extending 'user' with the required attributes assigned and many to many relationship with messages
class Member(User):

    GENDER = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other'),
    )

    name = models.TextField(default='Profile', max_length=64)
    image = models.ImageField(upload_to='profile_images', default=None)
    gender = models.TextField(default='Male', max_length=6, choices=GENDER)
    dob = models.TextField(default='', null=False)
    age = models.IntegerField(default=0)
    hobbies = models.TextField(default='', max_length=1024)
    liked = models.TextField(default='', max_length=64000)
    # A text field with the usernames of liked members for matching
    matched = models.TextField(default='', max_length=64000)
    # A text field with members whom the member in question has matched with
    messages = models.ManyToManyField(
        to='self',
        blank=True,
        symmetrical=False,
        through='Message',
        related_name='related_to'
    )

    def __str__(self):
        return self.username


# messages model
class Message(models.Model):
    sender = models.ForeignKey(
        to=Member,
        related_name='sent',
        on_delete=models.CASCADE
    )
    recipient = models.ForeignKey(
        to=Member,
        related_name='received',
        on_delete=models.CASCADE
    )
    text = models.CharField(max_length=4096)
    time = models.DateTimeField()

    def __str__(self):
        return 'From ' + self.sender.username
